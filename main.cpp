#include <stdio.h>
#include <iostream>
#include <Windows.h>
#include <conio.h>

#define MAX_NAME_LENGTH 28
#define MAX_DEV_LENGTH 28
#define MIN_NAME_LENGTH 2
#define MIN_DEV_LENGTH 2
#define MAX_PRICE 9999999.99
#define MIN_PRICE 0
#define MIN_YEAR 1947
#define MAX_YEAR 2016
#define MAX_LICENCE_LEN 15

using namespace std;

//��������� ����������� ���������

struct Prog
{
	char* 	name;
	char* 	developer;
	float 	price;
	int 	year;
	char*	licence;
};

//������� ��������� �������

//������ ��������� ����������� �������
void drawBorderElem(int color)
{ 
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, color);
	printf(" ");
	SetConsoleTextAttribute(hConsole, 11);
}
//������ ����� �� ��������� ���������
void drawLine()
{
	printf("\n          ");
	for(int i = 0; i < 100; i++)
		drawBorderElem(48);
	printf("\n          ");
}
//����� ���������
void drawCell(char* format, char* text)
{
	drawBorderElem(48);
	printf(format, text);
}
void drawHeader()
{
	drawLine();
	drawCell(" %-28s", "�������� ���������");
	drawCell(" %-28s", "�����������");
	drawCell(" %-12s", "����");
	drawCell(" %-6s", "���");
	drawCell(" %-15s", "��������");
	drawCell("","");
	drawLine();
}
//����� ����� ���������
void drawRow(Prog program)
{
	drawLine();
	drawBorderElem(48);
	printf(" %-28s", program.name);
	drawBorderElem(48);
	printf(" %-28s", program.developer);
	drawBorderElem(48);
	printf(" %-10.2f$ ", program.price);
	drawBorderElem(48);
	printf(" %d�.", program.year);
	drawBorderElem(48);
	printf(" %-15s", program.licence);
	drawBorderElem(48);
	drawLine();
}
//����� ������ ����
void drawMenuElem(char* label, int color)
{
	printf("\n                              ");
	drawBorderElem(color);
	printf(" %-57s",label);
	drawBorderElem(color);
	printf("\n");
}
//����� ����
void drawMenu(int activeElement)
{
	system("cls");
	char* elements[6] = {"���� ������","����� �������� ������","���������","����� ����������","�������","����� �� ���������"};
	printf("\n\n\n\n                              ");
	for(int i = 0; i < 60; i++)
		drawBorderElem(48);
	printf("\n                                                         ����\n                              ");
	for(int i = 0; i < 60; i++)
		drawBorderElem(48);
	printf("\n\n\n\n");
	for(int i = 0; i < 6; i++)
		drawMenuElem(elements[i],(i==activeElement)?192:48);
	printf("\n\n                              ����������� ������� �����/���� ��� ���������\n                              Enter ��� ������ ������ ����");
	
}
void drawSubMenu(int activeElement)
{
	system("cls");
	char* elements[3] = {"�������� �������� � ��������� � ������","�������� ������ �������� � ������������ �����","�����"};
	printf("\n\n\n\n                              ");
	for(int i = 0; i < 60; i++)
		drawBorderElem(48);
	printf("\n                                                 �������� �������� :\n                              ");
	for(int i = 0; i < 60; i++)
		drawBorderElem(48);
	printf("\n\n\n\n");
	for(int i = 0; i < 3; i++)
		drawMenuElem(elements[i],(i==activeElement)?192:48);
	
}

void addToList(Prog* list, int* listSize, Prog element)
{
	list[*listSize] = element;
	*listSize+=1;
}

char* readString(int maxLen,int minLen)
{
	char* string = NULL;
	int stringLen = 0;
	bool endOfInput = 0;
	while(stringLen < maxLen && !endOfInput)
	{
		char temp = getch();
		if(temp == 13)
			if(stringLen >= minLen)
				endOfInput = 1;
			else
			{
				printf("\n������ �� ����� ���� ������ %d ��������\n", minLen);
				printf("%s",string);
			}	
		else
		if(temp == 8)
		{
			printf("%c %c",temp,temp);
			if(stringLen > 1)
			{
				stringLen--;
				realloc(string,sizeof(char)*(stringLen+1));
				string[stringLen] = '\0';
			}
			else
			{
				stringLen=0;
				free(string);
				string = NULL;
			}
		}
		else
		{
			printf("%c",temp);
			stringLen++;
			string = (char*)realloc(string,sizeof(char) * (stringLen+1));
			string[stringLen-1] = temp;
			string[stringLen] = '\0';
		}
	}
printf("\n");
return string;
}

Prog progInput(){
	Prog temp = {};
	system("cls");
	printf("������� �������� ��������� [2-30 ��������]:\n");
	temp.name = readString(MAX_NAME_LENGTH, MIN_NAME_LENGTH);
	printf("������� ������������ ��������� [2-30 ��������]:\n");
	temp.developer = readString(MAX_DEV_LENGTH, MIN_DEV_LENGTH);
	printf("������� ���� ��������� � $ [0-9999999.99]:\n");
	do
	{
		scanf("%f",&temp.price);
		if(temp.price < MIN_PRICE || temp.price > MAX_PRICE) printf("���� �� ������ � �������� �������, ���������� ��������� ����.\n");
	}
	while(temp.price < MIN_PRICE || temp.price > MAX_PRICE);
	printf("������� ��� ������� ��������� � $ [1947-2016]:\n");
	do
	{
		scanf("%d",&temp.year);
		if(temp.year < MIN_YEAR || temp.year > MAX_YEAR) printf("��� �� ������ � �������� �������, ���������� ��������� ����.\n");
	}
	while(temp.year < MIN_YEAR || temp.year > MAX_YEAR);
	printf("������� ��� �������� ��������� [3-15 ��������]:\n");
	temp.licence = readString(MAX_LICENCE_LEN, 3);
	return temp;
}

void drawOutput(Prog* list, int listSize)
{
	printf("\n\n");
	int linesCnt = 5;
	int screen = 0;
	int temp = 0;
	int screenCnt = listSize/linesCnt+(listSize%linesCnt == 0)+1;
	do
	{
		system("cls");
		drawHeader();
		printf("\n");
		for(int i = screen*linesCnt; i < (screen+1)*linesCnt; i++)
			if(i < listSize) drawRow(list[i]);
		printf("\n\n         ����� %d �� %d. ����������� ������� <- � -> ��� ������������ �������, Ec� ��� ������",screen+1, screenCnt);
		temp = getch();
		if(temp == 75) screen = (screenCnt+(screen-1))%screenCnt;
		if(temp == 77) screen = (screen+1)%screenCnt;
	}
	while(temp != 27);
}

void showHint()
{
	system("cls");
	printf("��������� ������������� ��� ����� ������ � ��������� ���������� � ������ ��������\n");
	printf("� ������� ��� ������� � ���� ��������� � ���������� �������������.\n\n");
	printf("���������� � ��������� ������� �� 5 �����: \n");
	printf("��������\t[�� 2 �� 30 ��������]\n");
	printf("�����������\t[�� 2 �� 30 ��������]\n");
	printf("����\t\t[�� 0 �� 1000000$]\n");
	printf("��� �������\t[�� 1947 �� 2016]\n");
	printf("��� ��������\t[�� 3 �� 15 ��������]\n\n");
	printf("������� ����� ������� ��� �������� � ������� ����...");
	getch();
}

Prog* freeList(Prog* list, int* count)
{
	for(int i = 0; i < *count; i++)
	{
		free(list[i].developer);
		free(list[i].licence);
		free(list[i].name);
	}
	*count = 0;
	free(list);
	return NULL;
}

int main(){
	setlocale(LC_CTYPE, "Russian_Russia.1251");
	SetConsoleOutputCP(1251);
	SetConsoleCP(1251);
        
	Prog *listSource = NULL;
	int sourceSize = 0;
	Prog *listResult = NULL;
	int resultSize = 0;
	
	bool isExit = false;
	
	int activeElement = 0;
	int menuSize = 6;
	
	do
	{
		drawMenu(activeElement);
		
		char temp = getch();
		if(temp == 72) activeElement = (menuSize+(activeElement-1))%menuSize;
		if(temp == 80) activeElement = (activeElement+1)%menuSize;
		if(temp == 13)
		{
			switch(activeElement)
			{
				case 0: {
					bool isExit = false;
					int activeElement = 0;
					do
					{
						drawSubMenu(activeElement);
						int menuSize = 3;
						char temp = getch();
						if(temp == 72) activeElement = (menuSize+(activeElement-1))%menuSize;
						if(temp == 80) activeElement = (activeElement+1)%menuSize;
						if(temp == 13)
						{
							switch(activeElement)
							{
								//�������� ��������� � ������
								case 0:{
									listSource = (Prog*)realloc(listSource, sizeof(Prog)*(sourceSize+1));
									addToList(listSource, &sourceSize, progInput());
									break;
								}
								//������� ����� ������
								case 1:{
									listSource = freeList(listSource, &sourceSize);
									listResult = freeList(listResult, &resultSize);
									system("cls");
									printf("������ ������� ������.\n\n������� ����� ������� ��� �����������...");
									getch();
									break;
								}
								case 2: isExit = true;
							}
						}
					}
					while(!isExit);
					break;
				}
				//����� ��������� �������
				case 1:{
					if(sourceSize == 0)
					{
						system("cls");
						printf("�������� ������ �������� ����. ������� ���� ���� ���������.\n\n������� ����� ������� ��� �����������...");
						getch();
					}
					else
					drawOutput(listSource, sourceSize);
					break;
				}
				//���������
				case 2:{
					
					break;
				}
				//����� ����������
				case 3:{
					if(sourceSize == 0)
					{
						system("cls");
						printf("�������� ������ �������� ����. ������� ���� ���� ���������.\n\n������� ����� ������� ��� �����������...");
						getch();
					}
					else
					if(resultSize == 0)
					{
						system("cls");
						printf("�������� ������ �������� �� ���������. ����������� ������.\n\n������� ����� ������� ��� �����������...");
						getch();
					}
					else
					drawOutput(listResult, resultSize);
					break;
				}
				case 4:{
					showHint();
					break;
				}
				case 5: isExit = true;
			}
		}
	}
	while(!isExit);
	return 0;
}
